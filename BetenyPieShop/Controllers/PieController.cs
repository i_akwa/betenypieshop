﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BetenyPieShop.Models;
using BetenyPieShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BetenyPieShop.Controllers
{
    public class PieController : Controller
    {
        private readonly IPieRepository _pieRepository;
        private readonly ICategoryRepository _categoryRepository;

        public PieController(IPieRepository pieRepository, ICategoryRepository categoryRepository)  
        {
            _pieRepository = pieRepository;
            _categoryRepository = categoryRepository;
        }

        public ViewResult List()
        {
            PieListViewModel pieList = new PieListViewModel();
            pieList.Pies = _pieRepository.Pies;

            pieList.CurrentCategory = "Cake";

            ViewBag.CurrentCategory = "Cheese Cakes";
            return View(pieList);
        }
    }
}